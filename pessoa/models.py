from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext as _
from django.contrib import auth

from garagem.models import Garagem

class TipoPessoa:
    ADMINISTRADOR = "Administrador"
    CONSUMIDOR = "Consumidor"

    CHOICES = [
        (ADMINISTRADOR, _("Administrador")),
        (CONSUMIDOR, _("Consumidor")),
    ]


class Pessoa(AbstractUser):
    telefone = models.CharField(max_length=20)
    username = models.CharField(max_length=100, unique=True)
    email = models.CharField(max_length=100, unique=True)
    tipo_pessoa = models.CharField(max_length=20, choices=TipoPessoa.CHOICES)
    
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'email']

    @classmethod
    def postSaveForPessoa(cls, instance, created, **kwargs):
        if not created:
            return
        garagem = Garagem.objects.create(proprietario=instance, numero=instance.id)

    def save(self, *args, **kwargs):
        self.is_superuser = self.is_superuser or self.tipo_pessoa == TipoPessoa.ADMINISTRADOR
        self.is_staff = self.is_staff or self.tipo_pessoa == TipoPessoa.ADMINISTRADOR
        
        super(Pessoa, self).save(*args, **kwargs)


# Signals
post_save.connect(Pessoa.postSaveForPessoa, sender=Pessoa)