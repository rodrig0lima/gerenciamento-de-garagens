from rest_framework import serializers

from .models import Pessoa

class PessoaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Pessoa
        fields = ('first_name', 'last_name','telefone', 'email', 'tipo_pessoa', 'username', 'password')


