from django.urls import include, path
from django.conf.urls import url

from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token

from . import views

router = routers.DefaultRouter()
router.register(r'pessoa', views.PessoaViewSet)

urlpatterns = [
    path('', include(router.urls)),
    url('pessoa/list', views.get_clientes),
    url('pessoa/veiculos-alocados', views.get_clientes_veiculos_alocados),
    url('pessoa/veiculos-nao-alocados', views.get_clientes_veiculos_nao_alocados),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api-token-auth/', obtain_auth_token, name='api_token_auth'),
]