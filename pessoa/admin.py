from django.contrib import admin

from garagem.models import Garagem, Veiculo
from pessoa.models import Pessoa

# Garagem application

admin.site.register(Garagem)
admin.site.register(Veiculo)

# Pessoa application

admin.site.register(Pessoa)