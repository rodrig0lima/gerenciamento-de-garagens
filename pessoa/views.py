from django.contrib.auth.hashers import make_password
from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from garagem.models import Veiculo
from .models import Pessoa, TipoPessoa
from .serializers import PessoaSerializer


class PessoaViewSet(viewsets.ModelViewSet):
    queryset = Pessoa.objects.all().order_by('first_name')
    serializer_class = PessoaSerializer
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    permission_classes = [IsAdminUser]

    def create(self, request):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            serializer.validated_data['password'] = make_password(serializer.validated_data['password'])
            Pessoa.objects.create(**serializer.validated_data)
            return Response(serializer.validated_data)

        return badRequest('Usuário não pode ser criado com os dados enviados.')


# Específico para aplicações de terceiros

class GetClientes(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        clientes = Pessoa.objects.filter(tipo_pessoa=TipoPessoa.CONSUMIDOR)
        serializer = PessoaSerializer(clientes, context={'request':request}, many=True)
        return Response(serializer.data)


class GetClientesVeiculosAlocados(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        clientes = Pessoa.objects.filter(
            tipo_pessoa=TipoPessoa.CONSUMIDOR,
            veiculo__alocacao__isnull=False,
        ).distinct()
        serializer = PessoaSerializer(clientes, context={'request':request}, many=True)
        return Response(serializer.data)


class GetClientesVeiculosNaoAlocados(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):

        clientes = Pessoa.objects.filter(
            tipo_pessoa=TipoPessoa.CONSUMIDOR,
            veiculo__alocacao__isnull=True,
        ).distinct()
        serializer = PessoaSerializer(clientes, context={'request':request}, many=True)
        return Response(serializer.data)


get_clientes = GetClientes.as_view()
get_clientes_veiculos_alocados = GetClientesVeiculosAlocados.as_view()
get_clientes_veiculos_nao_alocados = GetClientesVeiculosNaoAlocados.as_view()
