from rest_framework import status
from rest_framework.response import Response

def badRequest(message):
    return Response({
            'status': 'Bad request',
            'message': message,
        }, status=status.HTTP_400_BAD_REQUEST)