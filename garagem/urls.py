from django.conf.urls import url
from django.urls import include, path

from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'garagem', views.GaragemViewSet)
router.register(r'veiculo', views.VeiculoConsumidorViewSet)
router.register(r'veiculo/admin', views.VeiculoAdministradorViewSet)

urlpatterns = [
    path('', include(router.urls)),
    url('veiculo/(?P<veiculo_id>[0-9]+)/get_info', views.get_info),
    url('veiculo/proprietario/alocados', views.alocados),
    url('veiculo/proprietario/nao_alocados', views.nao_alocados),
    url('garagem/ativa', views.garagens_ativas)
]