from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from gerenciamentogaragens.globals import badRequest
from pessoa.models import Pessoa
from .models import Garagem, Veiculo
from .serializers import GaragemSerializer, VeiculoSerializer


class GaragemViewSet(ModelViewSet):
    queryset = Garagem.objects.all()
    serializer_class = GaragemSerializer
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    permission_classes = [IsAdminUser]

class VeiculoAdministradorViewSet(ModelViewSet):
    queryset = Veiculo.objects.all().order_by('modelo')
    serializer_class = VeiculoSerializer
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    permission_classes = [IsAdminUser]


class VeiculoConsumidorViewSet(ModelViewSet):
    queryset = Veiculo.objects.all().order_by('modelo')
    serializer_class = VeiculoSerializer
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def list(self, request):
        veiculos = Veiculo.objects.filter(proprietario__id=request.user.id)
        serializer = VeiculoSerializer(veiculos, context={'request':request}, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            proprietario = Pessoa.objects.get(id=request.user.id)

            alocacao = serializer.validated_data['alocacao']
            if alocacao is not None:
                garagem_id = alocacao.id
                garagem = Garagem.objects.filter(id=garagem_id, proprietario__id=proprietario.id).all()
                if garagem.count() == 0:
                    return badRequest('A garagem não pertence a este proprietário.')
    
            Veiculo.objects.create(proprietario=proprietario, **serializer.validated_data)

            return Response(serializer.validated_data)

        return badRequest('Veiculo não pode ser criado com os dados enviados.')

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        proprietario = Pessoa.objects.get(id=request.user.id)

        if not instance.proprietario.id == proprietario.id:
            return badRequest('Veiculo não pertence a este proprietario.')

        serializer = self.get_serializer(instance)
        data = serializer.data
        return Response(data)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        proprietario = Pessoa.objects.get(id=request.user.id)
        
        if not instance.proprietario.id == proprietario.id:
            return badRequest('Veiculo não pertence a este proprietario.')

        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)

        self.perform_update(serializer)

        return Response(serializer.data)


class GetInfoVeiculo(APIView):
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, veiculo_id, *args, **kwargs):
        veiculo = Veiculo.objects.get(id=veiculo_id, proprietario__id=request.user.id)

        return Response(veiculo.get_info())


class GetVeiculosProprietarioNaoAlocados(APIView):
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        veiculos = Veiculo.objects.filter(proprietario__id=request.user.id, alocacao__isnull=True)
        serializer = VeiculoSerializer(veiculos, context={'request':request}, many=True)
        return Response(serializer.data)


class GetVeiculosProprietarioAlocados(APIView):
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = VeiculoSerializer

    def get(self, request, *args, **kwargs):
        veiculos = Veiculo.objects.filter(proprietario__id=request.user.id, alocacao__isnull=False)
        serializer = VeiculoSerializer(veiculos, context={'request':request}, many=True)
        return Response(serializer.data)


# Específico para aplicações de terceiros

class GetGaragensAtivas(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = GaragemSerializer

    def get(self, request, *args, **kwargs):
        garagens = Garagem.objects.filter(ativa=True)
        serializer = GaragemSerializer(garagens, context={'request':request}, many=True)
        return Response(serializer.data)


get_info = GetInfoVeiculo.as_view()
alocados = GetVeiculosProprietarioAlocados.as_view()
nao_alocados = GetVeiculosProprietarioNaoAlocados.as_view()
garagens_ativas = GetGaragensAtivas.as_view()