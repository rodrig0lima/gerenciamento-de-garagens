from rest_framework import serializers

from .models import Garagem, Veiculo


class GaragemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Garagem
        fields = '__all__'


class VeiculoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Veiculo
        fields = ('marca', 'modelo', 'cor', 'placa', 'ano', 'tipo_veiculo', 'alocacao')
