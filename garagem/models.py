from django.db import models
from django.utils.translation import gettext as _


class TipoVeiculo:
    CAMINHAO = "caminhão"
    CARRO = "carro"
    MOTO = "moto"
    CHOICES = [
        (CAMINHAO, _("Caminhão")),
        (CARRO, _("Carro")),
        (MOTO, _("Moto")),
    ]


class Garagem(models.Model):
    proprietario = models.OneToOneField("pessoa.Pessoa", unique=True, on_delete=models.CASCADE)
    endereco = models.CharField(max_length=100)
    ativa = models.BooleanField(default=True)

    def __str__(self):
        return self.proprietario.first_name+" "+self.proprietario.last_name


class Veiculo(models.Model):
    marca = models.CharField(max_length=60)
    modelo = models.CharField(max_length=60)
    cor = models.CharField(max_length=20)
    placa = models.CharField(max_length=8)
    ano = models.CharField(max_length=4)
    tipo_veiculo = models.CharField(max_length=10, choices=TipoVeiculo.CHOICES)
    proprietario = models.ForeignKey("pessoa.Pessoa", on_delete=models.CASCADE)
    alocacao = models.ForeignKey(Garagem, on_delete=models.CASCADE, null=True, blank=True)

    def get_info(self):
        if not self.tipo_veiculo == TipoVeiculo.MOTO:
            return (self.cor, self.ano)
        else:
            return (self.modelo, self.ano)
